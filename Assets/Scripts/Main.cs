﻿using UnityEngine;

public class Main : MonoBehaviour {

    private void Awake()
    {
        Services.Prefabs = Resources.Load<PrefabDB>("Config/PrefabDB");
        Services.RocketProps = Resources.Load<RocketProperties>("Config/RocketProperties");
    }


    private void Update ()
    {
        if (Input.anyKeyDown)
        {
            var mousePos = Input.mousePosition;
            mousePos.z = 10;
            Rocket.CreateAt(Camera.main.ScreenToWorldPoint(mousePos));
        }
    }

}
