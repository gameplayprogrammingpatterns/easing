﻿using System.Collections;

using GC;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    [SerializeField] private Transform _smokeEmissionPoint;

    public static void CreateAt(Vector3 position)
    {
        Instantiate(Services.Prefabs.Rocket, position, Quaternion.identity);
    }

    private void Start()
    {
        StartCoroutine(Animate());
    }

    private IEnumerator EmitSmoke()
    {
        while (gameObject.activeInHierarchy)
        {
            Instantiate(Services.Prefabs.RocketSmokePuff, _smokeEmissionPoint.position, Quaternion.identity);
            yield return new WaitForSeconds(Services.RocketProps.PufffEmissionRate);
        }
    }

    private IEnumerator Animate()
    {
        var startPosition = gameObject.transform.localPosition;
        var endPosition = startPosition + new Vector3(0, Services.RocketProps.RocketFlightDistance, 0);

        StartCoroutine(EmitSmoke());

        yield return StartCoroutine(Coroutines.DoOverEasedTime(Services.RocketProps.RocketFlightDuration, Services.RocketProps.MovementEasing, t =>
        {
            gameObject.transform.localPosition = Vector3.Lerp(startPosition, endPosition, t);
        }));

        Instantiate(Services.Prefabs.Explosion, transform.localPosition, Quaternion.identity);

        Destroy(gameObject);
    }

}