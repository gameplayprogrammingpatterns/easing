﻿
using System.Collections;
using UnityEngine;

public class Explosion : MonoBehaviour
{

    private IEnumerator DestroyWhenDone(ParticleSystem system)
    {
        while (system.isPlaying) yield return null;
        Destroy(gameObject);
    }

    private void Start()
    {
        var particles = GetComponent<ParticleSystem>();
        Debug.Assert(particles != null, "Explosion is missing a particle system");
        StartCoroutine(DestroyWhenDone(particles));
    }

}
