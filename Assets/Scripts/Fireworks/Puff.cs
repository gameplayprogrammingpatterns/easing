﻿using System.Collections;
using UnityEngine;

public class Puff : MonoBehaviour
{
	private void Start () {
	    var temp = Random.insideUnitCircle * Services.RocketProps.PuffOffsetRange;
	    var offset = new Vector3(temp.x, 0, temp.y);
	    transform.localPosition += offset;
	    StartCoroutine(Animate());
	}

    private IEnumerator Animate()
    {
        var duration = Random.Range(Services.RocketProps.MinPuffDuration, Services.RocketProps.MaxPuffDuration);

        // Animate the color
        var material = gameObject.GetComponent<Renderer>().material;
        StartCoroutine(Coroutines.DoOverEasedTime(duration, Services.RocketProps.ColorEasing, t =>
        {
            material.color = Color.Lerp(Services.RocketProps.PuffStartColor, Services.RocketProps.PuffEndColor, t);
        }));

        // Animate the scale
        var min = Services.RocketProps.MinPuffScale;
        var max = Services.RocketProps.MaxPuffScale;
        var minScale = new Vector3(min, min, min);
        var maxScale = new Vector3(max, max, max);

        // first we scale up...
        var upDuration = duration * Services.RocketProps.PuffScaleUpProportion;
        yield return StartCoroutine(Coroutines.DoOverTime(upDuration, t =>
        {
            gameObject.transform.localScale = Vector3.Lerp(minScale, maxScale, t);
        }));

        // Then scale down...
        var downDuration = duration - upDuration;
        yield return StartCoroutine(Coroutines.DoOverTime(downDuration, t =>
        {
            gameObject.transform.localScale = Vector3.Lerp(maxScale, minScale, t);
        }));

        Destroy(gameObject);
    }

}
