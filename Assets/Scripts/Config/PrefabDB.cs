﻿
using UnityEngine;

[CreateAssetMenu (menuName = "Prefab DB")]
public class PrefabDB : ScriptableObject
{
    [SerializeField] private GameObject _rocket;
    public GameObject Rocket {get { return _rocket; }}

    [SerializeField] private GameObject _explosion;
    public GameObject Explosion {get { return _explosion; }}

    [SerializeField] private GameObject _rocketSmokePuff;
    public GameObject RocketSmokePuff {get { return _rocketSmokePuff; }}
}
