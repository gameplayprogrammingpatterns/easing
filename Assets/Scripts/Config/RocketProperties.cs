﻿using GC;
using UnityEngine;

[CreateAssetMenu (menuName = "Smoke Properties")]
public class RocketProperties : ScriptableObject
{
    [SerializeField] private float _pufffEmissionRate;
    public float PufffEmissionRate {get { return _pufffEmissionRate; }}

    [SerializeField] private float _minPuffDuration;
    public float MinPuffDuration {get { return _minPuffDuration; }}

    [SerializeField] private float _maxPuffDuration;
    public float MaxPuffDuration {get { return _maxPuffDuration; }}

    [SerializeField] private float _minPuffScale;
    public float MinPuffScale {get { return _minPuffScale; }}

    [SerializeField] private float _maxPuffScale;
    public float MaxPuffScale {get { return _maxPuffScale; }}

    [SerializeField] private float _puffOffsetRange;
    public float PuffOffsetRange {get { return _puffOffsetRange; }}

    [SerializeField] private Color _puffStartColor;
    public Color PuffStartColor {get { return _puffStartColor; }}

    [SerializeField] private Color _puffEndColor;
    public Color PuffEndColor {get { return _puffEndColor; }}

    [Range(0.1f, 0.9f)] [SerializeField] private float _puffScaleUpProportion;
    public float PuffScaleUpProportion {get { return _puffScaleUpProportion; }}

    [SerializeField] private Easing.FunctionType _puffColorEasing;
    public Easing.Function ColorEasing { get { return Easing.GetFunctionWithTypeEnum(_puffColorEasing); }}

    [SerializeField] private float _rocketFlightDistance;
    public float RocketFlightDistance { get { return _rocketFlightDistance; }}

    [SerializeField] private float _rocketFlightDuration;
    public float RocketFlightDuration { get { return _rocketFlightDuration; }}

    [SerializeField] private Easing.FunctionType _rocketMovementEasing;
    public Easing.Function MovementEasing { get { return Easing.GetFunctionWithTypeEnum(_rocketMovementEasing); }}
}
